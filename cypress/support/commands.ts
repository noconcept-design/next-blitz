Cypress.Commands.add("signup", ({ email, password }) => {
  cy.contains("a", /Rezeption/i).click();

  cy.contains("Email").find("input").type(email);
  cy.contains("Passwort").find("input").type(password);
  cy.contains("button", /Einchecken/i).click();
});

Cypress.Commands.add("login", ({ email, password }) => {
  cy.contains("a", /Schlüsselkarte/i).click();

  cy.contains("Email").find("input").type(email);
  cy.contains("Passwort").find("input").type(password);
  cy.contains("button", /Schlüsselkarte/i).click();
});

export {};
