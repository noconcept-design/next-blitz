import { createRandomUser } from "../support/helpers";

describe("index page", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("goes to the signup page", () => {
    cy.contains("a", /Rezeption/i).click();
    cy.location("pathname").should("equal", "/rezeption");
  });

  it("goes to the login page", () => {
    cy.contains("a", /Schlüsselkarte/i).click();
    cy.location("pathname").should("equal", "/schluesselkarte");
  });

  it("allows the user to signup", () => {
    const user = createRandomUser();

    cy.signup(user);

    cy.location("pathname").should("equal", "/");
    cy.contains("button", /Abmelden/i);
  });

  it("allows the user to login", () => {
    const user = createRandomUser();

    cy.signup(user);

    cy.contains("button", /Abmelden/i).click();
    cy.contains("a", /Schlüsselkarte/i).click();

    cy.contains("Email").find("input").type(user.email);
    cy.contains("Passwort").find("input").type(user.password);
    cy.contains("button", /Schlüsselkarte/i).click();

    cy.location("pathname").should("equal", "/schluesselkarte");
    cy.contains("button", /Abmelden/i);
  });

  it("allows the user to logout", () => {
    const user = createRandomUser();

    cy.signup(user);

    cy.contains("button", /Abmelden/i).click();

    cy.location("pathname").should("equal", "/");
  });

  // it("tracks anonymous sessions", () => {
  //   // TODO - why does this fail on windows??
  //   cy.skipOn("windows")
  //   const user = createRandomUser()

  //   cy.contains("button", "Track view").click()
  //   cy.contains("button", "Track view").click()
  //   cy.contains('"views": 2')

  //   cy.signup(user)

  //   cy.contains('"views": 2')
  // })
});

export {};
