const { sessionMiddleware, unstable_simpleRolesIsAuthorized } = require("@blitzjs/server");

module.exports = {
  middleware: [
    sessionMiddleware({
      unstable_isAuthorized: unstable_simpleRolesIsAuthorized,
    }),
  ],
  /* Uncomment this to customize the webpack config
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    // Important: return the modified config
    return config
  },
  */
  images: {
    deviceSizes: [320, 420, 768, 1024, 1200],
    imageSizes: [16, 32, 64],
    domains: [
      "images.unsplash.com",
      "images.pexels.com",
      "images.ctfassets.net",
      "placekitten.com",
      "placebear.com",
      "picsum.photos",
      "www.stevensegallery.com",
      "localhost",
    ],
    path: "/_next/image",
    loader: "default",
  },
};
