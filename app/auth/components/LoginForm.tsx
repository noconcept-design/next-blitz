import React from "react";
import { AuthenticationError, Link, useMutation } from "blitz";
import { LabeledTextField } from "app/components/LabeledTextField";
import { Form, FORM_ERROR } from "app/components/Form";
import login from "app/auth/mutations/login";
import { LoginInput } from "app/auth/validations";

type LoginFormProps = {
  onSuccess?: () => void;
};

export const LoginForm = (props: LoginFormProps) => {
  const [loginMutation] = useMutation(login);

  return (
    <div>
      <h1>Schlüsselbrett</h1>

      <Form
        submitText="Schlüsselkarte"
        schema={LoginInput}
        initialValues={{ email: "", password: "" }}
        onSubmit={async (values) => {
          try {
            await loginMutation(values);
            props.onSuccess?.();
          } catch (error) {
            if (error instanceof AuthenticationError) {
              return { [FORM_ERROR]: "Oops, die Daten sind ungültig" };
            } else {
              return {
                [FORM_ERROR]: "Opps - interner Fehler, wird sofort behoben!. - " + error.toString(),
              };
            }
          }
        }}
      >
        <LabeledTextField name="email" label="Email" placeholder="@" />
        <LabeledTextField
          name="password"
          label="Passwort"
          placeholder="Schatz123"
          type="password"
        />
      </Form>

      <div style={{ marginTop: "1rem" }}>
        Bei der <Link href="/rezeption">Rezeption</Link> anmelden.
      </div>
    </div>
  );
};

export default LoginForm;
