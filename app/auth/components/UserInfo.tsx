import { useCurrentUser } from "app/hooks/useCurrentUser";
import logout from "app/auth/mutations/logout";
import { Link, useMutation } from "blitz";

const UserInfo = () => {
  const currentUser = useCurrentUser();
  const [logoutMutation] = useMutation(logout);

  if (currentUser) {
    return (
      <>
        <button
          className="button small"
          onClick={async () => {
            await logoutMutation();
          }}
        >
          Abmelden
        </button>
        <div>
          ID Schlüsselkarte: <code>{currentUser.id}</code>
          <br />
          Schlüsselrechte: <code>{currentUser.role}</code>
        </div>
      </>
    );
  } else {
    return (
      <>
        <Link href="/rezeption">
          <a className="button small">Rezeption</a>
        </Link>
        <Link href="/schluesselkarte">
          <a className="button small">Schlüsselkarte</a>
        </Link>
      </>
    );
  }
};

export default UserInfo;
