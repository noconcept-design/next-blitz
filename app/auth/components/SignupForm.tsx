import React from "react";
import { useMutation } from "blitz";
import { LabeledTextField } from "app/components/LabeledTextField";
import { Form, FORM_ERROR } from "app/components/Form";
import signup from "app/auth/mutations/signup";
import { SignupInput } from "app/auth/validations";

type SignupFormProps = {
  onSuccess?: () => void;
};

export const SignupForm = (props: SignupFormProps) => {
  const [signupMutation] = useMutation(signup);

  return (
    <div>
      <h1>Rezeption</h1>

      <Form
        submitText="Einchecken"
        schema={SignupInput}
        initialValues={{ email: "", password: "" }}
        onSubmit={async (values) => {
          try {
            await signupMutation(values);
            props.onSuccess?.();
          } catch (error) {
            if (error.code === "P2002" && error.meta?.target?.includes("email")) {
              // This error comes from Prisma
              return { email: "Opps - Sie sind schon registriert ?!?" };
            } else {
              return { [FORM_ERROR]: error.toString() };
            }
          }
        }}
      >
        <LabeledTextField name="email" label="Email" placeholder="@" />
        <LabeledTextField
          name="password"
          label="Passwort"
          placeholder="Schatz123"
          type="password"
        />
      </Form>
    </div>
  );
};

export default SignupForm;
