import React from "react";
import { useRouter, BlitzPage } from "blitz";
import Layout from "app/layouts/Layout";
import { LoginForm } from "app/auth/components/LoginForm";

const SchluesselkartePage: BlitzPage = () => {
  const router = useRouter();

  return (
    <div>
      <LoginForm onSuccess={() => router.push("/")} />
    </div>
  );
};

SchluesselkartePage.getLayout = (page) => <Layout title="Schlüsselkarte">{page}</Layout>;

export default SchluesselkartePage;
