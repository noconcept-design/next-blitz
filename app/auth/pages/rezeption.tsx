import React from "react";
import { useRouter, BlitzPage } from "blitz";
import Layout from "app/layouts/Layout";
import { SignupForm } from "app/auth/components/SignupForm";

const RezeptionPage: BlitzPage = () => {
  const router = useRouter();

  return (
    <div>
      <SignupForm onSuccess={() => router.push("/")} />
    </div>
  );
};

RezeptionPage.getLayout = (page) => <Layout title="Einchecken">{page}</Layout>;

export default RezeptionPage;
