import { BlitzPage } from "blitz";
import Layout from "app/layouts/Layout";
// import logout from "app/auth/mutations/logout";
// import { useCurrentUser } from "app/hooks/useCurrentUser";
import { Suspense } from "react";
import UserInfo from "app/auth/components/UserInfo";

/*
 * This file is just for a pleasant getting started page for your new app.
 * You can delete everything in here and start from scratch if you like.
 */

// const UserInfo = () => {
//   const currentUser = useCurrentUser();
//   const [logoutMutation] = useMutation(logout);

//   if (currentUser) {
//     return (
//       <>
//         <button
//           className="button small"
//           onClick={async () => {
//             await logoutMutation();
//           }}
//         >
//           Abmelden
//         </button>
//         <div>
//           ID Schlüsselkarte: <code>{currentUser.id}</code>
//           <br />
//           Schlüsselrechte: <code>{currentUser.role}</code>
//         </div>
//       </>
//     );
//   } else {
//     return (
//       <>
//         <Link href="/rezeption">
//           <a className="button small">Rezeption</a>
//         </Link>
//         <Link href="/schluesselkarte">
//           <a className="button small">Schlüsselkarte</a>
//         </Link>
//       </>
//     );
//   }
// };

const Home: BlitzPage = () => {
  return (
    <div>
      <p>
        <strong>Willkommen</strong> in
        <a
          href="https://www.ted.com/talks/jeff_dekofsky_the_infinite_hotel_paradox?utm_campaign=tedspread&utm_medium=referral&utm_source=tedcomshare"
          target="_blank"
          rel="noopener noreferrer"
        >
          {" "}
          <s>Hilberts</s> OJEs Hotel!
        </a>
      </p>
      {/* <div className="buttons" style={{ marginTop: "1rem", marginBottom: "1rem" }}>
        <Suspense fallback="Senf wird geladen...">
          <UserInfo />
        </Suspense>
      </div> */}
    </div>
  );
};

Home.getLayout = (page) => <Layout title="Home">{page}</Layout>;

export default Home;
