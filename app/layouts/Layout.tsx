import { ReactNode, Suspense } from "react";
import { Head, Link } from "blitz";
import Image from "next/image";
import UserInfo from "app/auth/components/UserInfo";

type LayoutProps = {
  title?: string;
  children: ReactNode;
};

const Layout = ({ title, children }: LayoutProps) => {
  return (
    <>
      <Head>
        <title>{title || "OJE.ooo"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="flex flex-col max-w-screen-lg h-100 justify-between mx-auto">
        <header className="flex flex-row">
          <div>
            <Link href="/">
              <a>
                <h1 className="font-extrabold text-purple-900 tracking-tighter text-xl py-4">
                  <Image
                    className="logo"
                    src="/assets/images/TO-LOGO.svg"
                    alt="LOGO"
                    width={96}
                    height={97}
                  />
                  OJE.ooo
                </h1>
              </a>
            </Link>
          </div>

          <nav className="ml-auto flex flex-row items-center">
            <Link href="/products/1">
              <a>Empfehlungen</a>
            </Link>

            <Suspense fallback="Daten werden geladen...">
              <UserInfo />
            </Suspense>
          </nav>
        </header>
        {children}

        <div className="buttons" style={{ marginTop: "5rem" }}>
          <a
            className="button-outline"
            href="https://vercel.com/noconcept-design/next-blitz"
            target="_blank"
            rel="noopener noreferrer"
          >
            Vercel
          </a>
          <a
            className="button-outline"
            href="https://github.com/oje-edu/next-blitz"
            target="_blank"
            rel="noopener noreferrer"
          >
            Github
          </a>
          <a
            className="button-outline"
            href="https://oje.ooo/start"
            target="_blank"
            rel="noopener noreferrer"
          >
            OJE.ooo
          </a>
        </div>

        <footer className="h-10 bg-purple-900">
          <a href="mailto:ooo.ejo@oje.ooo">©OJE.ooo</a>
          <p className="footer-corp">-gefördert durch-</p>
          <a href="https://noconcept.design" target="_blank" rel="noopener noreferrer">
            NOCONCEPT.DESIGN
          </a>
          <p className="footer-corp">-einer-</p>
          <a href="https://mindblast.space" target="_blank" rel="noopener noreferrer">
            MINDBLAST.SPACE Gruppe.
          </a>
        </footer>
      </div>
    </>
  );
};

export default Layout;
