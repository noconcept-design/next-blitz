import React from "react";
import LabeledTextField from "app/components/LabeledTextField";
import { Form, FORM_ERROR } from "app/components/Form";

type RequestFormProps = {
  initialValues: any;
  onSubmit: (values: any) => void;
};

const RequestForm = ({ initialValues, onSubmit }: RequestFormProps) => {
  return (
    <Form
      submitText="Vorschlag erstellen"
      //schema={LoginInput}
      initialValues={{ title: "" }}
      onSubmit={async (values) => {
        try {
          onSubmit(values);

          //await loginMutation(values);
          //props.onSuccess?.();
        } catch (error) {
          if (error.name === "AuthenticationError") {
            return { [FORM_ERROR]: "Oops, die Daten sind ungültig" };
          } else {
            return {
              [FORM_ERROR]: "Opps - interner Fehler, wird sofort behoben!. - " + error.toString(),
            };
          }
        }
      }}
    >
      <LabeledTextField name="title" label="Titel" placeholder="Titel" />
      <LabeledTextField
        name="description"
        label="Inhalt"
        placeholder="Gib deine Empfehlung ein...."
      />
    </Form>
  );
};

export default RequestForm;
